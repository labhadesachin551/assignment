import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  message:string;
  usersList=[];
  
  constructor(private fb: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      rememberMe:['']
    });
    let token=sessionStorage.getItem('user');
    console.log(token)
    let remember=JSON.parse(localStorage.getItem('remember'));
    if(token!=null)
    {
      this.router.navigate(['/dashboard']);
    }else{
      if(remember){
        sessionStorage.setItem('user',remember.name);
        this.router.navigate(['/dashboard']);
      }
    }

  }


  get f() {
    return this.loginForm.controls;
  }

  login() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.usersList=JSON.parse(localStorage.getItem('users'));
    if(this.usersList!=null){
      let index=this.usersList.findIndex((ele)=>{
        return ele.email==this.loginForm.value.email && ele.password==this.loginForm.value.password
      })
      if(index != -1){
        this.message='Login Successfull';
        sessionStorage.setItem('user',this.loginForm.value.email)
        if(this.loginForm.value.rememberMe){
          localStorage.setItem('remember',JSON.stringify({'name':this.loginForm.value.email}));
        }
      }else{
        this.message='Invalid credentials';
        return;
      }
      setTimeout(()=>{
        this.message=null;
        this.loginForm.reset();
        this.router.navigate(['/dashboard']);
      },1500)

    }else{
      this.message='Please register first';
    }
  }

}
