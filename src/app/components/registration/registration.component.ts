import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  message:string;
usersList:any;
  constructor(private fb: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      name:['',Validators.required],
      email: ["", [Validators.required,Validators.email]],
      password: ["", [Validators.required,Validators.minLength(8)]],
     cpassword: ["", Validators.required],
      accepttnc:[false, Validators.requiredTrue]

    });
    this.usersList=JSON.parse(localStorage.getItem('users'));
    console.log(this.usersList)
    if(!this.usersList)
    {
      let usersList=[]
      localStorage.setItem('users', JSON.stringify(usersList))
    }

  }


  get f() {
    return this.registerForm.controls;
  }

  login() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    
    let userDetails={...this.registerForm.value}
    delete userDetails['cpassword'];
    delete userDetails['accepttnc'];
   
    if(this.usersList!=null){
      let index=this.usersList.findIndex((ele)=>{
        return ele.email==userDetails.email
      })
      if(index != -1){
        this.message='Email is already exist, use different';
        return;
      }
    }
      this.usersList.push(userDetails)
      localStorage.setItem('users',JSON.stringify(this.usersList))
      this.message='You have register successfully';
      setTimeout(()=>{
        this.message=null;
        this.registerForm.reset();
        this.router.navigate(['/login']);
      },1500)

    
  }
}
