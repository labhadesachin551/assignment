import { Component, OnInit } from '@angular/core';
import { PostService } from '../../core/services/post.service';
import { Router } from '@angular/router'
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
posts:any;
post:any;
index:number
postForm: FormGroup;
  submitted = false;
  editPostStatus:boolean=false
  message:string;
  originialPost:any;
  constructor(private api:PostService,private fb: FormBuilder,private router:Router) { }

  ngOnInit(): void {
   this.validations();
    this.getAllPost();


    if(!sessionStorage.getItem('user'))
    {
      this.router.navigate(['/login']);
    }

  }

  get f() {
    return this.postForm.controls;
  }

  validations(){
    this.postForm = this.fb.group({
      title: ["", Validators.required],
      body: ["", Validators.required],
      userId:['1'],
      id: [""],
    });
  }
  createPost(){
    this.submitted=false
    this.post=null;
    this.validations;
    this.postForm.reset()
  }

  deletePost(){
    this.api.deletePost(this.post.id).subscribe(res=>{
 
      this.originialPost.splice(this.index,1)
     this.posts=this.originialPost;
    
     delete this.post
     this.editPostStatus=false
  });

  }

  editPost(){
    this.postForm.patchValue(this.post)
  }


  getAllPost(){
    this.api.getAllPost().subscribe(res=>{
        this.posts=res;
        this.originialPost=res;
    })
  }

  singlePost(post,index){
   this.index=index
    this.post=post;
    this.postForm.patchValue(post)
  }



  postAddEdit(){
   
    this.submitted = true;
        // stop here if form is invalid
    if (this.postForm.invalid) {
      return;
    }
    let post={...this.postForm.value}
    if(post.id)
    {
    
        if(!this.index){this.index=0}
        this.message='Post updated successfully';
        this.originialPost[this.index]=post;
        this.posts=this.originialPost;
        this.post=post;
        this.editPostStatus=false;
        this.clearMessage()
    }else{
      delete post.id;
      this.api.createPost(post).subscribe(res=>{
        this.message='Post created successfully';
        this.originialPost.unshift(res)
        this.posts=this.originialPost;
        this.post=res;
        this.editPostStatus=false;
        this.clearMessage()
    })
    }

   
  }

  searchPost(val){
    if(!val){
      this.posts=this.originialPost
    }
  let filterPost= this.originialPost.filter(ele=>{
      return ele.title.includes(val)
    })
    if(filterPost.length!=0)
    {
      this.posts=filterPost;
    }else{
      this.posts=[];
    }

  }

  clearMessage(){
    setTimeout(()=>{
      this.message='';
    },2000)
  }

}
