import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  public base_url: any;

  constructor( public http:HttpClient) {
    this.base_url = environment.base_url;
  }

  getAllPost()
  {
    return this.http.get<any>(this.base_url + "posts");
  }

  createPost(post){
    return this.http.post<any>(this.base_url + "posts",post);
  }

  updatePost(post){
    return this.http.post<any>(this.base_url + "posts/"+post.id,post);
  }

  
  deletePost(post){
    return this.http.delete<any>(this.base_url + "posts/"+post.id);
  }

  
}
