import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
user:string;
  constructor(private router:Router) { }

  ngOnInit(): void {
this.user=sessionStorage.getItem('user');
  }

  logout(){
    sessionStorage.removeItem('user');
    localStorage.removeItem('remember');
    this.router.navigate(['/login'])
  }

}
